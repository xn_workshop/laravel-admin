<?php

namespace Xn\Admin\Controllers\Traits;
trait AuthMethod
{

    public function refreshCaptcha()
    {
        return captcha_img(config('admin.auth.captcha_config'));
    }


    public function userAuthMethod($account)
    {
        $userModel = config('admin.database.users_model');
        $user = (new $userModel)->where('username', $account)->first();
        if ($user) {
            return response()->json([
                'code' => '0',
                'auth_method' => $user->auth_method
            ]);
        } else {
            return response()->json([
                'code' => '0',
                'auth_method' => 'none'
            ]);
        }
    }

    /**
     * OTP Auth
     *
     * @param array $data
     * @return void
     */
    protected function otpAuthValidate(array $data) {
        $account = $data[$this->username()];
        $secret = $data['auth_otp'];
        $userModel = config('admin.database.users_model');
        $user = (new $userModel)->where('username', $account)->first();
        if ($user) {
            $google2fa = (new \PragmaRX\Google2FAQRCode\Google2FA());
            return $google2fa->verifyKey($user->google2fa_secret, $secret);
        }
        return false;
    }
}
