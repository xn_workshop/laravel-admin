<?php

namespace Xn\Admin\Controllers\Traits;


trait Common
{

    /**
     * 认证方式
     *
     * @return array
     */
    static function authMethod() {
        $options = [
            'none' => trans('None'),
            'otp' => trans('OTP'),
        ];

        if (!config('captcha.disable')) {
            $options['captcha'] = trans('Captcha');
        }

        return $options;
    }

    static function switchLocalize($type = 'public') {
        $data = [
            'radio_option' => [
                '1' => __('admin.YES'),
                '0' => __('admin.NO')
            ],
            'open' => [
                'on'  => ['value' => 1, 'text' => __('admin.YES'), 'color' => 'success'],
                'off' => ['value' => 0, 'text' => __('admin.NO'), 'color' => 'warning'],
            ],
            'public' => [
                'on'  => ['value' => 1, 'text' => __('admin.OPEN'), 'color' => 'primary'],
                'off' => ['value' => 0, 'text' => __('admin.CLOSE'), 'color' => 'danger'],
            ],
            'html' => [
                '1' => "<span class='label label-large label-warning'>".__('admin.YES')."</span>",
                '0' => "<span class='label label-large label-default'>".__('admin.NO')."</span>",
            ],
        ];

        return $data[$type];
    }
}
