<?php

namespace Xn\Admin\Show;

class Divider extends Field
{
    public function render()
    {
        return '<hr>';
    }
}
