<?php

namespace Xn\Admin\Helper;

use Doctrine\DBAL\Connection as DoctrineConnection;
use Doctrine\DBAL\Driver\PDO\MySQL\Driver as MysqlDriver;
use Doctrine\DBAL\Driver\PDO\PgSQL\Driver as PgSQLDriver;
use Doctrine\DBAL\Driver\PDO\SQLite\Driver as SQLiteDriver;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\MySQL;
use Illuminate\Database\Schema\Builder as SchemaBuilder;

class DatabaseDoctrine
{
    /**
     * The instance of Doctrine connection.
     *
     * @var \Doctrine\DBAL\Connection
     */
    protected $doctrineConnection;

    public function __construct($config)
    {
        $dbalDriver = strtolower($config['driver']);
        switch ($dbalDriver) {
            case 'mysql':
                $dbalDriver = 'pdo_mysql';
                break;
            case 'pgsql':
                $dbalDriver = 'pdo_pgsql';
                break;
            case 'sqlite':
                $dbalDriver = 'pdo_sqlite';
                break;
            case 'sqlsrv':
                $dbalDriver = 'pdo_sqlsrv';
                break;
        }

        $this->doctrineConnection = DriverManager::getConnection([
            'dbname' => $config['database'],
            'user' => $config['username'],
            'password' => $config['password'],
            'host' => $config['host'],
            'driver' => $dbalDriver,
        ]);
    }

    /**
     * Get the Doctrine DBAL schema manager for the connection.
     *
     * @return \Doctrine\DBAL\Schema\AbstractSchemaManager
     */
    public function getDoctrineSchemaManager()
    {
        return $this->getDoctrineConnection()->createSchemaManager();
    }

    /**
     * Get the Doctrine DBAL database connection instance.
     *
     * @return \Doctrine\DBAL\Connection
     */
    public function getDoctrineConnection()
    {
        return $this->doctrineConnection;
    }
}
