<?php

namespace Xn\Admin\Layout;

interface Buildable
{
    public function build();
}
