<?php

namespace Xn\Admin\Grid\Filter;

use Illuminate\Support\Arr;

class In extends AbstractFilter
{
    /**
     * {@inheritdoc}
     */
    protected $query = 'whereIn';

    /**
     * Get condition of this filter.
     *
     * @param array $inputs
     *
     * @return mixed
     */
    public function condition($inputs)
    {
        $value = Arr::get($inputs, $this->column);

        if (is_null($value)) {
            return;
        }

        if (is_string($value) && strpos($value, ",")) {
            $value = explode(",", $value);
        }

        $this->value = (array) $value;

        return $this->buildCondition($this->column, $this->value);
    }
}
