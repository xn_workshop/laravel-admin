<?php

namespace Xn\Admin\Grid\Displayers;

use Xn\Admin\Facades\Admin;

class Collapsible extends AbstractDisplayer
{
    protected function addStyle()
    {
        $css = <<<CSS
        .collapsible {
            position: relative;
            overflow: hidden;
            transition: max-height 0.3s ease-out;
        }
        .collapsible.collapsed {
            max-height: 3em;
        }
        .collapsible .col-content {
            padding-right: 20px;
        }
        .toggle-btn {
            position: absolute;
            top: 0;
            right: 0;
            background-color: #f0f0f0cc;
            border: none;
            padding: 2px 5px;
            cursor: pointer;
            display: none;
        }
        .table-responsive>.table>tbody>tr>td:has(.collapsible) {
            white-space: normal !important;
        }
CSS;

        Admin::style($css);
    }

    protected function addScript()
    {
        $script = <<<SCRIPT
document.querySelectorAll('.collapsible').forEach(cell => {
    const content = cell.querySelector('.col-content');
    const btn = cell.querySelector('.toggle-btn');

    function checkOverflow() {
        const isOverflowing = content.clientHeight > 48; // 3em ≈ 48px
        btn.style.display = isOverflowing ? 'block' : 'none';
        cell.classList.toggle('collapsed', isOverflowing);
    }

    checkOverflow();

    window.addEventListener('resize', checkOverflow);

    btn.addEventListener('click', () => {
        cell.classList.toggle('collapsed');
        btn.classList.contains('fa-angle-double-down') ?
            btn.classList.replace('fa-angle-double-down', 'fa-angle-double-up') :
            btn.classList.replace('fa-angle-double-up', 'fa-angle-double-down');
    });
});
SCRIPT;

        Admin::script($script);
    }

    public function display()
    {
        $this->addStyle();

        $this->addScript();

        return <<<HTML
<div class="collapsible">
    <div class="col-content">{$this->getValue()}</div>
    <a class="fas fa-angle-double-down toggle-btn"></a>
</div>
HTML;
    }
}
