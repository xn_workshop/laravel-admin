<?php

namespace Xn\Admin\Grid\Displayers;

use Xn\Admin\Admin;

class FancyBox extends AbstractDisplayer
{
    /**
     * @param string $title
     * @param string $type
     * @param \Closure|string $callback
     * @param array $paramaters
     *
     * @return mixed|string
     */
    public function display($type = 'image', $width = '120', $height = 'auto')
    {
        return Admin::component('admin::components.column-fancybox', [
            'key'   => $this->getName().'_'.$this->getKey(),
            'type'  => $type,
            'value' => $this->value,
            'width' => $width,
            'height' => $height,
        ]);
    }
}
