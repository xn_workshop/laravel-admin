<?php


namespace Xn\Admin\Grid\Displayers;


class BtnActions extends Actions
{
    protected $btnSize = 'xs';

    public function setBtnSize($size)
    {
        $this->btnSize = $size;
    }

    public function viewName()
    {
        return __('admin.view');
    }

    public function editName()
    {
        return __('admin.edit');
    }

    public function deleteName()
    {
        return __('admin.delete');
    }

    /**
     * Render view action.
     *
     * @return string
     */
    protected function renderView()
    {
        return <<<EOT
<a href="{$this->getResource()}/{$this->getRouteKey()}" class="{$this->grid->getGridRowName()}-view btn btn-{$this->btnSize} btn-info">
    <i class="fa fa-eye"></i> {$this->viewName()}
</a>
EOT;
    }

    /**
     * Render edit action.
     *
     * @return string
     */
    protected function renderEdit()
    {
        return <<<EOT
<a href="{$this->getResource()}/{$this->getRouteKey()}/edit" class="{$this->grid->getGridRowName()}-edit btn btn-{$this->btnSize} btn-warning">
    <i class="fa fa-edit"></i> {$this->editName()}
</a>
EOT;
    }

    /**
     * Render delete action.
     *
     * @return string
     */
    protected function renderDelete()
    {
        $this->setupDeleteScript();

        return <<<EOT
<a href="javascript:void(0);" data-id="{$this->getKey()}" class="{$this->grid->getGridRowName()}-delete btn btn-{$this->btnSize} btn-danger">
    <i class="fa fa-trash"></i> {$this->deleteName()}
</a>
EOT;
    }

}
