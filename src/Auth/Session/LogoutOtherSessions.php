<?php

namespace Xn\Admin\Auth\Session;

use Illuminate\Auth\Events\Login;

class LogoutOtherSessions
{
    public function handle(Login $event)
    {
        $sessionModel = config('admin.database.sessions_model');

        $user = $event->user;

        try {
            // 删除除当前会话外的所有用户会话
            $sessionModel::where('user_id', $user->id)
            ->where('id', '!=', session()->getId())
            ->delete();
        } catch (\Throwable $th) {
            //throw $th;
        }
    }
}
