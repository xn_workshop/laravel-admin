<?php

namespace Xn\Admin\Auth\Database;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create a user.
        Administrator::truncate();
        Administrator::create([
            'username' => 'admin',
            'password' => Hash::make('admin'),
            'name'     => 'Administrator',
            'removable' => '0',
            'status' => 1,
        ]);

        // create a role.
        Role::truncate();
        Role::create([
            'name' => 'Administrator',
            'slug' => 'administrator',
        ]);

        // add role to user.
        Administrator::first()->roles()->save(Role::first());

        //create a permission
        Permission::truncate();
        Permission::insert([
            [
                'name'        => 'All permission',
                'slug'        => '*',
                'http_method' => '',
                'http_path'   => '*',
            ],
            [
                'name'        => 'Dashboard',
                'slug'        => 'dashboard',
                'http_method' => 'GET',
                'http_path'   => '/',
            ],
            [
                'name'        => 'Login',
                'slug'        => 'auth.login',
                'http_method' => '',
                'http_path'   => "/auth/login\r\n/auth/logout",
            ],
            [
                'name'        => 'User setting',
                'slug'        => 'auth.setting',
                'http_method' => 'GET,PUT',
                'http_path'   => '/auth/setting',
            ],
            [
                'name'        => 'Auth management',
                'slug'        => 'auth.management',
                'http_method' => '',
                'http_path'   => "/auth/roles\r\n/auth/permissions\r\n/auth/menu\r\n/auth/logs",
            ],
            [
                'name'        => 'Auth',
                'slug'        => 'auth',
                'http_method' => '',
                'http_path'   => "",
            ],
        ]);

        Role::first()->permissions()->save(Permission::first());

        // add default menus.
        Menu::truncate();
        Menu::insert([
            [
                'parent_id' => 0,
                'order'     => 1,
                'title'     => 'Dashboard',
                'icon'      => 'fas fa-tachometer-alt',
                'uri'       => '/',
            ],
            [
                'parent_id' => 0,
                'order'     => 2,
                'title'     => 'Setting',
                'icon'      => 'fas fa-cogs',
                'uri'       => '',
            ],
            [
                'parent_id' => 0,
                'order'     => 3,
                'title'     => 'Manage',
                'icon'      => 'fas fa-tasks',
                'uri'       => '',
            ],
            [
                'parent_id' => 3,
                'order'     => 4,
                'title'     => 'Users',
                'icon'      => 'fas fa-users',
                'uri'       => 'auth/users',
            ],
            [
                'parent_id' => 3,
                'order'     => 5,
                'title'     => 'Roles',
                'icon'      => 'fas fa-user',
                'uri'       => 'auth/roles',
            ],
            [
                'parent_id' => 3,
                'order'     => 6,
                'title'     => 'Permission',
                'icon'      => 'fas fa-ban',
                'uri'       => 'auth/permissions',
            ],
            [
                'parent_id' => 3,
                'order'     => 7,
                'title'     => 'Menu',
                'icon'      => 'fas fa-bars',
                'uri'       => 'auth/menu',
            ],
            [
                'parent_id' => 3,
                'order'     => 8,
                'title'     => 'Operation log',
                'icon'      => 'fas fa-history',
                'uri'       => 'auth/logs',
            ],
            [
                'parent_id' => 2,
                'order'     => 1,
                'title'     => 'Locale supports',
                'icon'      => 'fab fa-edge',
                'uri'       => 'auth/locale',
            ],
            [
                'parent_id' => 2,
                'order'     => 2,
                'title'     => 'Time zones',
                'icon'      => 'fas fa-globe',
                'uri'       => 'auth/timezone',
            ],
            [
                'parent_id' => 2,
                'order'     => 3,
                'title'     => 'Language lines',
                'icon'      => 'fas fa-language',
                'uri'       => 'auth/language_lines',
            ],
        ]);

        // add role to menu.
        Role::first()->menus()->attach(Menu::pluck('id'));

        // locale
        LocaleSupport::truncate();
        LocaleSupport::insert([
            [
                'code'          => 'zh-CN',
                'name'          => '简中',
                'status'        => '0',
                'sort_order'    => 0,
            ],
            [
                'code'          => 'zh-HK',
                'name'          => '繁中',
                'status'        => '1',
                'sort_order'    => 0,
            ],
            [
                'code'          => 'en',
                'name'          => 'English',
                'status'        => '1',
                'sort_order'    => 0,
            ],
        ]);

        // timezone
        TimeZone::truncate();
        TimeZone::insert([
            [
                'name'          => 'UTC +8',
                'timezone'      => 'Asia/Shanghai',
                'status'        => '1',
                'sort_order'    => 0,
            ],
            [
                'name'          => 'UTC +0',
                'timezone'      => 'UTC',
                'status'        => '1',
                'sort_order'    => 0,
            ],
        ]);
    }
}
