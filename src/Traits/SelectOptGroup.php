<?php

namespace Xn\Admin\Traits;

trait SelectOptGroup
{

    public function scopeGroups($query, $childrenRelation = 'children', $keyColumn = 'code', $labelColumn = 'name')
    {
        $options = $query->with($childrenRelation)->get()->toArray();
        $group = [];
        foreach($options as $opt) {
            if (is_array($opt[$childrenRelation]) && !empty($opt[$childrenRelation])) {
                $option = [
                    'label' => $opt[$labelColumn],
                    'options' => []
                ];
                foreach($opt[$childrenRelation] as $_opt) {
                    $option['options'][$_opt[$keyColumn]] = $_opt[$labelColumn];
                }

                $group[] = $option;
            }
        }

        return $group;
    }
}
