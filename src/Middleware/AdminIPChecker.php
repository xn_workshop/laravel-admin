<?php

namespace Xn\Admin\Middleware;

use Closure;
use Illuminate\Http\Request;
use Xn\Admin\Controllers\AuthController;
use Xn\Admin\Facades\Admin;
use Xn\Admin\Helper\XNCache;

class AdminIPChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (config('admin.ip_whitelist') !== false) {
            if (isset(Admin::user()->username)) {
                $whitelistIP = explode(",", config('admin.ip_whitelist'));
                $ip = $request->getClientIp();
                if (!XNCache::IpContainChecker($ip, $whitelistIP)) {
                    $auth = new AuthController();
                    return $auth->getLogout($request, "IP Access denied: {$ip}");
                }
            }
        }
        return $next($request);
    }
}
