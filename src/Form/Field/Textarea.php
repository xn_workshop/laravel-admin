<?php

namespace Xn\Admin\Form\Field;

use Xn\Admin\Admin;
use Xn\Admin\Form\Field;

class Textarea extends Field
{
    use PlainInput;
    use HasValuePicker;

    /**
     * Default rows of textarea.
     *
     * @var int
     */
    protected $rows = 5;

    /**
     * Set rows of textarea.
     *
     * @param int $rows
     *
     * @return $this
     */
    public function rows($rows = 5)
    {
        $this->rows = $rows;

        return $this;
    }

    /**
     * Max Rows
     *
     * @return $this
     */
    public function limitMaxRows()
    {
        $name = $this->variables()['name'];
        $script = <<<EOF
        $("[name='$name']").on('keypress', function(){
            var textarea = $(this),
            text = textarea.val(),
            numberOfLines = (text.match(/\\n/g) || []).length + 1,
            maxRows = parseInt(textarea.attr('rows'));
            if (event.which === 13 && numberOfLines >= maxRows ) {
                return false;
            }
        });
    EOF;
        $this->script[] = $script;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {
        if (!$this->shouldRender()) {
            return '';
        }

        if (is_array($this->value)) {
            $this->value = json_encode($this->value, JSON_PRETTY_PRINT);
        }

        $this->mountPicker(function ($btn) {
            $this->addPickBtn($btn);
        });

        $this->defaultAttribute('id', $this->id)
        ->defaultAttribute('name', $this->elementName ?: $this->formatName($this->column))
        ->defaultAttribute('value', old($this->column, $this->value()))
        ->defaultAttribute('class', 'form-control '.$this->getElementClassString())
        ->defaultAttribute('placeholder', $this->getPlaceholder());

        return parent::fieldRender([
            'append' => $this->append,
            'rows'   => $this->rows,
        ]);
    }

    /**
     * @param string $wrap
     */
    protected function addPickBtn($btn)
    {
        $style = <<<'STYLE'
.textarea-picker {
    padding: 5px;
    border-bottom: 1px solid #d2d6de;
    border-left: 1px solid #d2d6de;
    border-right: 1px solid #d2d6de;
    border-bottom-left-radius: 5px;
    border-bottom-right-radius: 5px;
    background-color: #f1f2f3;
}

.textarea-picker .btn {
    padding: 5px 10px;
    font-size: 12px;
    line-height: 1.5;
}
STYLE;
        Admin::style($style);

        $this->append = <<<HTML
<div class="text-right textarea-picker">
    {$btn}
</div>
HTML;

        return $this;
    }
}
