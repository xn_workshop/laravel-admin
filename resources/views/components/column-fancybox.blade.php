@switch($type)
    @case('image')
        <a href="{{ $value }}" data-fancybox data-caption="{{ $value }}">
            <img src="{{ $value }}" class="img img-thumbnail" width="{{ $width }}" height="{{ $height }}" />
        </a>
        <a href="{{ $value }}" data-fancybox="{{ $type != 'image' ? 'gallery' : '' }}" data-caption="{{ $value }}" style="display: none">
            <img src="{{ $value }}" class="img img-thumbnail" width="{{ $width }}" height="{{ $height }}" />
        </a>
    @break

    @case('gallery')
    @case('images')
        @if(is_array($value))
        @foreach($value as $k => $v)
            <a href="{{ $v }}" data-fancybox="gallery" data-caption="{{ $v }}"  {!! $k > 0 ? ' style="display: none"' : '' !!} >
                <img src="{{ $v }}" class="img img-thumbnail" width="{{ $width }}" height="{{ $height }}" />
            </a>
        @endforeach
        @else
            <a href="{{ $value }}" data-fancybox="gallery" data-caption="{{ $value }}">
                <img src="{{ $value }}" class="img img-thumbnail" width="{{ $width }}" height="{{ $height }}" />
            </a>
        @endif
    @break

    @case('video')
        <a data-fancybox href="{{ $value }}" data-width="{{ $width }}" data-height="{{ $height }}">
            <i class="fas fa-layer-group"></i>
        </a>
    @break

    @case('url')
    @case('iframe')
        <a data-fancybox data-type="iframe" data-src="{{ $value }}" data-width="{{ $width }}" data-height="{{ $height }}" href="javascript:;">
            <i class="fas fa-layer-group"></i>
        </a>
    @break

    @case('html')
        <a data-fancybox data-src="#fcy-{{ $key }}" href="javascript:;">
            <i class="fas fa-layer-group"></i>
        </a>

        <div style="display: none;" id="fcy-{{ $key }}">
            {!! $value !!}
        </div>
    @break

@endswitch


<script>
</script>
