<style>
    .nav-tabs {
        border-bottom: none;
    }
    .nav-tabs > li:hover > i{
        display: inline;
    }
    .nav-tabs-custom {
        box-shadow: none;
    }
    .close-tab {
        position: absolute;
        font-size: 10px;
        top: 2px;
        right: 5px;
        color: #94A6B0;
        cursor: pointer;
        display: none;
    }
    .header .add {
        font-size: large;
    }
    @media (min-width: 767px) {
        .has-many-tab-title {
            text-align: right;
        }
    }
</style>

<div class="form-group">
    <div id="has-many-{{$column}}" class="has-many-{{$column}}">
        <div class="header">
            <div class="col-md-2 {{$viewClass['label']}}"><h4 class="has-many-tab-title">{{ $label }}</h4></div>
            <div class="col-md-8 {{$viewClass['field']}}">
                <button type="button" class="btn btn-sm add"><i class="fa fa-plus-circle"></i></button>
            </div>
        </div>

        <hr style="clear: both;">

        <div class="nav-tabs-custom col-sm-offset-2 col-sm-8">
            <ul class="nav nav-tabs">
                @foreach($forms as $pk => $form)
                    <li class="@if ($form == reset($forms)) active @endif ">
                        <a href="#{{ $relationName . '_' . $pk }}" data-toggle="tab">
                            {{ $pk }} <i class="fa fa-exclamation-circle text-red hide"></i>
                        </a>
                        <i class="close-tab fa fa-times" ></i>
                    </li>
                @endforeach
            </ul>

            <div class="tab-content has-many-{{$column}}-forms">

                @foreach($forms as $pk => $form)
                    <div class="tab-pane fields-group has-many-{{$column}}-form @if ($form == reset($forms)) active @endif" id="{{ $relationName . '_' . $pk }}">
                        @foreach($form->fields() as $field)
                            {!! $field->render() !!}
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>
        <template class="nav-tab-tpl">
            <li class="new">
                <a href="#{{ $relationName . '_new_' . \Xn\Admin\Form\NestedForm::DEFAULT_KEY_NAME }}" data-toggle="tab">
                    &nbsp;new_{{ \Xn\Admin\Form\NestedForm::DEFAULT_KEY_NAME }} <i class="fa fa-exclamation-circle text-red hide"></i>
                </a>
                <i class="close-tab fa fa-times" ></i>
            </li>
        </template>
        <template class="pane-tpl">
            <div class="tab-pane fields-group new" id="{{ $relationName . '_new_' . \Xn\Admin\Form\NestedForm::DEFAULT_KEY_NAME }}">
                {!! $template !!}
            </div>
        </template>

    </div>

    <hr style="clear: both;">
</div>