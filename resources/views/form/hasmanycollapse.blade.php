<style>
    td .form-group {
        margin-bottom: 0 !important;
    }
    @media (min-width: 767px) {
        .has-many-collapse-title {
            text-align: right;
        }
    }
</style>

<div class="form-group">
    <div class="{{$viewClass['label']}}"><h4 class="has-many-collapse-title">{{ $label }}</h4></div>
    <div class="{{$viewClass['field']}}"></div>

    <div class="col-12">
        <hr style="margin-top: 0px; clear: both">
    </div>

    <div class="col-sm-offset-2 col-sm-8" style="margin-bottom: 16px">
        <div id="has-many-{{$column}}" class="has-many-{{$column}}">

            <div class="has-many-{{$column}}-forms" id="{{$column}}">

                @foreach($forms as $pk => $form)

                    <div class="has-many-{{$column}}-form fields-group">
                        <div class="box box-default">
                            <div class="box-header with-border">
                                @if(str_contains($pk, 'new_'))
                                    <a class="box-title" data-widget="collapse">{{$pk}}</a>
                                @else
                                    <a class="box-title" data-widget="collapse">###{{$pk}}###</a>
                                @endif
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                    @if($options['allowDelete'])
                                        <button type="button" class="btn btn-box-tool remove"><i class="fa fa-times"></i></button>
                                    @endif
                                </div>
                            </div>
                            <div class="box-body">
                                @foreach($form->fields() as $field)
                                    {!! $field->render() !!}
                                @endforeach
                            </div>
                        </div>
                    </div>

                @endforeach
            </div>

            <template class="{{$column}}-tpl">
                <div class="has-many-{{$column}}-form fields-group">
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <a class="box-title" data-widget="collapse">new___LA_KEY__</a>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            {!! $template !!}
                        </div>
                    </div>
                </div>
            </template>


            @if($options['allowCreate'])
                <div class="add btn btn-success btn-sm"><i class="fa fa-save"></i>&nbsp;{{ trans('admin.new') }}</div>
            @endif
        </div>
    </div>

    <hr style="clear: both">
</div>