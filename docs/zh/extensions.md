## Grid Row編輯彈窗
```php
<?php

namespace App\Admin\Extensions;

use Illuminate\Database\Eloquent\Model;
use Xn\Admin\Actions\RowModal;

class ReplyButton extends RowModal
{
    public $name = 'RowEditor';

    public function handle(Model $model)
    {
        // \Log::info($this->getKey());
        // // 下面的代码获取到上传的文件，然后使用`maatwebsite/excel`等包来处理上传你的文件，保存到数据库
        // // $request->file('file');
        // $id = $this->getKey();
        // $model->title = request()->input('title');
        // $model->save();
        return $this->response()->success("{$model->id}导入完成！")->refresh();
    }

    public function form()
    {
        // $this->hidden('id')->value($this->getKey());
        $this->text('title', __('標題'))->value($this->getRow()->slug);
        $this->textarea('message', __('內文'))->value($this->getRow()->merchant_code);
    }
}
```
使用
```php
    $grid->actions(function ($actions) {
        $actions->append(new ReplyButton);
    });
```

## Grid Column Modal
```php
<?php

namespace App\Admin\Extensions;

use App\Models\Platform\Marquee;
use Xn\Admin\Grid\Filter;
use Xn\Admin\Grid\Selectable;

class PopupForm extends Selectable
{

    public $model = Marquee::class;

    public function make()
    {
        $this->column('agent.name', __('agent'));
        $this->column('merchant.name', __('merchant'));

        $this->filter(function (Filter $filter) {
            $filter->disableIdFilter();
            $filter->like('agent.name');
        });

        $this->model()->where('id', request()->get('key'));
    }

}
```
使用
```php
    $grid->column('content', __('content'))->modal(PopupForm::class);
```

## Grid Column Layer

使用
```php
    # layer('標題', '類型', '內容', [參數])
    $grid->column('content', __('content'))->layer('標題', 'iframe', 'http://127.0.0.1', []);
```

## Action Layer
```php

<?php

namespace App\Admin\Actions\Marquee;

use App\Admin\Controllers\Platform\MarqueeController;
use Xn\Admin\Actions\ActionLayer;
use Illuminate\Http\Request;
use Xn\Admin\Admin;
use App\Admin\Controllers\Traits\Common;
use App\Models\Company;
use App\Models\Platform\Marquee;
use App\Models\Platform\MarqueeTemplate;
use Xn\Admin\Actions\Action;
use Xn\Admin\Grid;
use Xn\Admin\Widgets\Form;

class LayerAction extends ActionLayer
{
    use Common;

    public $name = 'LayerAction';

    protected $selector = '.layer-action';

    protected $size = ['800px', 'auto']; // auto 自適應高度

    public function handle(Request $request)
    {
        \Log::info($request->all());
        // // $request ...
        // $templateId = $request->input('template_id');
        // $agentCode = $request->input('agent_code');
        // $template = MarqueeTemplate::find($templateId);
        // if ($template) {
        //     Marquee::create([
        //         'agent_code' => $agentCode,
        //         'lang' => $template->lang,
        //         'slug' => $template->slug,
        //         'content' => $template->content,
        //         'sort_order' => 0,
        //         'created_by' => \Admin::user()->username
        //     ]);
        //     return $this->response()->success('產出中, 请稍后')->refresh();
        // } else {
            return $this->response()->warning('查无范本');
        // }
    }

    protected function testGrid() {
        $grid = new Grid(new Marquee);

        $grid->column('agent.name', __('agent'));
        $grid->column('merchant.name', __('merchant'));
        $grid->column('status', __('form.status'))->switch(static::formSwitchLocale('public'));
        $grid->column('sort_order', __('form.sort_order'));
        $grid->column('lang', __('lang'));
        $grid->column('created_by', __('form.created_by'));
        $grid->model()->orderBy('sort_order')->orderBy('id', 'desc');

        $grid->disableActions()->disableColumnSelector()->disableCreateButton();
        // filter
        $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->column(1/2, function ($filter) {
                $filter->equal('agent_code', __('agent'))->select(Company::where('type', 'agent')->pluck('name', 'code'));
                $filter->equal('merchant_code', __('merchant'))->select(Company::where('type', 'merchant')->pluck('name', 'code'));
            });
            $filter->column(1/2, function ($filter) {
                $filter->equal('slug', __('slug'))->select(MarqueeController::formMarqueeTypeLocale());
                $filter->equal('lang', __('lang'))->select(MarqueeController::formLangLocale());
            });
        });

        return $grid;
    }

    protected function testForm() {
        $url = route('api.get-merchants');
        Admin::script("
        $(document).off('change', '.agent_code');
$(document).on('change', '.agent_code', function () {
    var target = $('select.merchant_code');
    $.get('$url',{q : this.value}, function (data) {
        target.find('option').remove();
        $(target).select2({
            allowClear: true,
            data: $.map(data, function (d) {
                d.id = d.id;
                d.text = d.text;
                return d;
            })
        });
    });
});
        ");

        $form = new Form();
        $form->text('test_1', 'TEST');
        $form->select('template_id', __('template'))->options(MarqueeTemplate::pluck('title', 'id'))->rules('required');
        $form->select('agent_code', __('agent'))->options(Company::where('type', 'agent')->pluck('name', 'code'))->load('merchant_code', route('api.get-merchants'))->rules('required');
        $form->select('merchant_code', __('merchant'))->options(Company::where('type', 'merchant')->pluck('name', 'code'));
        return $form;
    }
    // 實作Grid / Form
    public function make()
    {
        return $this->testGrid();
    }

    public function html()
    {
        return <<<HTML
        <a class="btn btn-sm btn-default layer-action">LayerAction</a>
HTML;
    }
}

```
使用
```php
    // tools
    $grid->tools(function (Grid\Tools $tools) {
        ...
        $tools->append(new LayerAction());
    });
```

# Layer Button
```php
  new LayerButton([
    HomeController::class,  // 1. 類別
    'getMember',            // 2. 公開function
    MerchantDatabase::class // 3.middleware（選填）
    ], 'InnerButton', 'default');
                   // 
 (new LayerButton([HomeController::class, 'button', MerchantDatabase::class], 'TEST2', 'danger'))->size('800px', '900px')
```
使用
```php
<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
...
use Xn\Admin\Widgets\LayerButton;

class HomeController extends Controller
{

    public function getMember() {
        $grid = new Grid(new Member());

        $grid->column('picture_url', __('頭像'))->width('100')->image(null, 50);
        $grid->column('account', __('帳號'));
        $grid->column('display_name', __('顯示名稱'));

        $grid->column('balance', '點數')->display(function($v){
            return floatval($v);
        })->sortable();
        // $grid->column('daily_max_withdraw','每日下分')->display(function($v){
        //     return floatval($v);
        // });

        $grid->column('updated_time', __('更新日期'))->sortable();

        $grid->model()->where('merchant_code', session('merchant_code'));

        $grid->disableCreateButton();
        // filter
        $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->like('account', '帳號');
            // $filter->selectGroup('merchant_code', __('商戶代碼'))->options(Company::where('type', 'agent')->with('merchants')->get())->value('code');

        });

        Admin::html($grid->render()); // 重要

        return view('admin::layer'); // 重要
    }

    public function button() {
        $widget = new LayerButton([HomeController::class, 'getMember', MerchantDatabase::class], 'InnerButton', 'default');

        Admin::html($widget->render());

        $widget = new LayerButton([HomeController::class, 'getMember', MerchantDatabase::class], 'InnerButton', 'warning');

        Admin::html($widget->render()); // 重要

        return view('admin::layer'); // 重要
    }

    public function index(Content $content)
    {
        return $content
            ->title('Dashboard')
            ->description('Description...')
            ->row(Dashboard::title())
            ->row(function (Row $row) {

                $row->column(4, function (Column $column) {
                    $column->append(new LayerButton([HomeController::class, 'getMember', MerchantDatabase::class], 'TEST', 'info'));
                });

                $row->column(4, function (Column $column) {
                    $column->append((new LayerButton([HomeController::class, 'button', MerchantDatabase::class], 'TEST2', 'danger'))->size('800px', '900px'));
                });

            });
    }
}
```
也可以在view中使用
```php
<div class="links">
    {!! (new \Xn\Admin\Widgets\LayerButton([\App\Admin\Controllers\HomeController::class, 'button', \App\Http\Middleware\MerchantDatabase::class], 'TEST2', 'danger'))->size('800px', '900px') !!}
    <a href="http://laravel-admin.org/docs"  target="_blank">Documentation</a>
    <a href="http://laravel-admin.org/demo"  target="_blank">Demo</a>
</div>
```
## Grid_Filter 多階層篩選器
```php
    $filter->in('bet_id', __('單號'))->multipleSelectTree(
        [
            [
                'id'=> '9STVG65WLGA2ECXG0CP',
                'title'=> '9STVG65WLGA2ECXG0CP'
            ], [
                'id'=> '9STVG65WLGA2ECXG0CN',
                'title'=> '9STVG65WLGA2ECXG0CN',
                'subs'=> [
                    [
                        'id'=> '9STVG65WLGA2ECXG0CM',
                        'title'=> '9STVG65WLGA2ECXG0CM'
                    ], [
                        'id'=> 11,
                        'title'=> 'choice 2 2'
                    ], [
                        'id'=> 12,
                        'title'=> 'choice 2 3'
                    ]
                ]
            ], [
                'id'=> 2,
                'title'=> 'choice 3'
            ], [
                'id'=> 3,
                'title'=> 'choice 4'
            ], [
                'id'=> 4,
                'title'=> 'choice 5'
            ], [
                'id'=> 5,
                'title'=> 'choice 6',
                'subs'=> [
                    [
                        'id'=> 50,
                        'title'=> 'choice 6 1'
                    ], [
                        'id'=> 51,
                        'title'=> 'choice 6 2',
                        'subs'=> [
                            [
                                'id'=> 510,
                                'title'=> 'choice 6 2 1'
                            ], [
                                'id'=> 511,
                                'title'=> 'choice 6 2 2'
                            ], [
                                'id'=> 512,
                                'title'=> 'choice 6 2 3'
                            ]
                        ]
                    ]
                ]
            ], [
                'id'=> 6,
                'title'=> 'choice 7'
            ]
        ]
    );
```